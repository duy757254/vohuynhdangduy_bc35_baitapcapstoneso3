import { HTTPS } from "./ConfigURL";

// Api Đăng Nhập
export const postLogin = (data) => {
  return HTTPS.post(`/api/QuanLyNguoiDung/DangNhap`, data);
};

// Api Đăng Ký

export const postRegister = (data) => {
  return HTTPS.post(`/api/QuanLyNguoiDung/DangKy`, data);
};
