import { HTTPS } from "./ConfigURL";

export const getDetailMovie = (data) => {
  return HTTPS.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};
