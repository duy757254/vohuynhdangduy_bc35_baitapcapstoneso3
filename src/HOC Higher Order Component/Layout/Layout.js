import React from "react";
import Header from "../../Component/Header/Header";

export default function Layout({ children }) {
  return (
    <div>
      <Header />

      <div className="mt-20">{children}</div>
    </div>
  );
}
