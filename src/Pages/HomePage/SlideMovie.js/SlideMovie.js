import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { slideMovie } from "../../../Service/MovieService";
  
export default function SlideMovie() {
  const [slide, setSlide] = useState([]);

  useEffect(() => {
    slideMovie()
      .then((res) => {
        setSlide(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderSlide = () => {
    return slide.map((banner) => {
      console.log("banner: ", banner);
      return (
        <div>
          <h3 style={contentStyle}>
            <img className="w-full h-full" src={banner.hinhAnh} alt="" />
          </h3>
        </div>
      );
    });
  };

  const contentStyle = {
    height: "600px",
    color: "#fff",
    lineHeight: "0px",
    textAlign: "left",
    background: "#364d79",
  };

  return (
    <div>
      <Carousel autoplay effect="scrollx">
        {renderSlide()}
      </Carousel>
    </div>
  );
}
