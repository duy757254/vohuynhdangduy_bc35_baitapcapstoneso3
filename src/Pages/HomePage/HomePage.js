import React from "react";
import { Carousel } from "antd";
import SlideMovie from "./SlideMovie.js/SlideMovie";
import ListMovie from "./ListMovie/ListMovie";

export default function HomePage() {
  return (
    <div>
      <div className="mb-10">
        <SlideMovie />
      </div>
      <h3 className="mx-24 my-10 text-3xl font-semibold border-b-2 z-10 hover:mx-10 duration-700 px-5 py-2  ">
        Danh Sách Phim
      </h3>
      <div>
        <ListMovie />
      </div>
    </div>
  );
}
