import React, { useState } from "react";
import { Button, Form, Input, message, Select } from "antd";
import Lottie from "lottie-react";
import animateBanner from "../../Acset/34590-movie-theatre.json";
import Bg_Login from "../../Acset/BG_Login.jpg";
import { NavLink, useNavigate } from "react-router-dom";
import { postRegister } from "../../Service/UserService";
import { useDispatch } from "react-redux";
import { setUserInfor } from "../../ReduxToolkit/UserSlice";
export default function RegisterPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const { Option } = Select;
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("values: ", values);
    postRegister(values)
      .then((res) => {
        message.success("Đăng Ký Thành Công");
        navigate("/login");
        dispatch(setUserInfor(res.data.content));
      })
      .catch((err) => {
        message.error("Đăng Ký Chưa Thành Công! Vui Lòng Kiểm Tra Lại");
        console.log(err);
      });
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="84">
          {" "}
          <span className="font-medium">+84</span>
        </Option>
      </Select>
    </Form.Item>
  );

  return (
    <div className="h-screen w-screen  ">
      <img className="absolute bg-opacity-75  -z-10" src={Bg_Login} alt="" />
      <div className="h-screen fixed opacity-90 w-screen px-80 py-32">
        <div className="bg-[#F56EB3] px-10 rounded-3xl flex justify-center items-center ">
          <div className="w-1/2">
            <Lottie animationData={animateBanner} />
          </div>
          <div>
            <Form
              className="py-10"
              {...formItemLayout}
              form={form}
              name="register"
              onFinish={onFinish}
              scrollToFirstError
            >
              <Form.Item
                className="font-medium"
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Tài Khoản ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                className="font-medium"
                name="matKhau"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Mật Khẩu ,
                      </div>
                    ),
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                className="font-medium"
                name="email"
                label="E-mail"
                rules={[
                  {
                    type: "email",
                    message: (
                      <div className="text-blue-500 font-bold">
                        Đây Không Phải là Email VD:(@gmail.com,
                        @yahoo.com,.....) ,
                      </div>
                    ),
                  },
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập E-mail ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                name="hoTen"
                label="Tên Tài Khoản"
                tooltip="Tên Tài Khoản Được Hiển Thị"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Tên Tài Khoản ,
                      </div>
                    ),
                    whitespace: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                name="soDt"
                label="Số Điện Thoại"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Số Điện Thoại ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input
                  addonBefore={prefixSelector}
                  style={{
                    width: "100%",
                  }}
                />
              </Form.Item>

              <Form.Item {...tailFormItemLayout}>
                <Button
                  className="bg-[#22A39F] font-medium text-white mr-2  "
                  htmlType="submit"
                >
                  Đăng Ký
                </Button>
                <Button className="bg-red-500 font-medium text-white  ">
                  <NavLink to={"/login"}>Đăng Nhập</NavLink>
                </Button>
              </Form.Item>
              <NavLink className=" " to={"/"}>
                <button className="text-center font-black  bg-[#F3EFE0] ml-28 rounded-3xl px-5 py-2 hover:text-[#22A39F] duration-1000">
                  Về Lại Trang Chủ Nhấn Vào Đây
                </button>
              </NavLink>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
