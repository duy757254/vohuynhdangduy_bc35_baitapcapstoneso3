import React, { useEffect, useState } from "react";
import { movieTheater, showTimesMovie } from "../../Service/MovieService";
import { Tabs } from "antd";
import { NavLink, useParams } from "react-router-dom";
import moment from "moment";

export default function DetailTab() {
  const [dataMovie, setdataMovie] = useState([]);
  let param = useParams();

  useEffect(() => {
    movieTheater(param.idPhim)
      .then((res) => {
        setdataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const onChange = (key) => {
    console.log(key);
  };

  const renderHeThongRap = () => {
    return dataMovie.heThongRapChieu?.map((heThongRap) => {
      return {
        label: (
          <div>
            <img className="w-20 h-20" src={heThongRap.logo} alt="" />,
          </div>
        ),

        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderThongTinPhim(heThongRap)}
          />
        ),
      };
    });
  };

  const renderThongTinPhim = (heThongRap) => {
    return heThongRap?.cumRapChieu?.map((thongTinPhim) => {
      console.log("thongTinPhim: ", thongTinPhim);
      return {
        label: (
          <div className="text-left">
            <h1 className="font-black">{thongTinPhim.tenCumRap}</h1>
            <br />
            <p>{thongTinPhim.diaChi}</p>
          </div>
        ),

        key: thongTinPhim.maCumRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderLichChieu(thongTinPhim)}
          />
        ),
      };
    });
  };

  const renderLichChieu = (thongTinPhim) => {
    return thongTinPhim.lichChieuPhim.map((lichChieu) => {
      console.log("lichChieu: ", lichChieu);
      return {
        label: (
          <div className="flex items-center mt-10 space-x-5">
            <h3 className="font-black">Lịch Chiếu Phim:</h3>
            <div className="px-5 py-2 bg-[#EAE7B1] rounded-lg">
              {moment(lichChieu.ngayChieuGioChieu).format("LLLL")}
            </div>
            <NavLink>
              <button className="px-5 py-2 bg-[#A6BB8D] text-black font-bold rounded-2xl hover:bg-[#EAE7B1] duration-1000 ">
                Mua Vé
              </button>
            </NavLink>
          </div>
        ),

        key: lichChieu.maLichChieu,
      };
    });
  };

  return (
    <div className="mt-10 mx-24 p-10 bg-[#22A39F] flex items-center  hover:drop-shadow-2xl duration-1000  rounded-2xl">
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
