import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import animateBanner from "../../Acset/34590-movie-theatre.json";
import Bg_Login from "../../Acset/BG_Login.jpg";
import { postLogin } from "../../Service/UserService";
import { useDispatch } from "react-redux";
import { setUserInfor } from "../../ReduxToolkit/UserSlice";
import { NavLink, useNavigate } from "react-router-dom";
import { userLocalService } from "../../Service/LocalService";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("values: ", values);
    postLogin(values)
      .then((res) => {
        console.log("res: ", res);
        message.success("Đăng nhập thành công");
        userLocalService.set(res.data.content);
        dispatch(setUserInfor(res.data.content));
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng Nhập Thất Bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="h-screen w-screen  ">
      <img className="absolute bg-opacity-75  -z-10" src={Bg_Login} alt="" />
      <div className="h-screen fixed opacity-90 w-screen px-80 py-32">
        <div className="bg-[#F56EB3] px-10 rounded-3xl flex justify-center items-center ">
          <div className="w-1/2">
            <Lottie animationData={animateBanner} />
          </div>
          <div className="w-1/2">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 15,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                className="font-medium"
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Tài Khoản ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                label="Mật Khẩu"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Xin Vui Lòng Nhập Mật Khẩu",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 8,
                }}
              >
                <div className="flex gap-2">
                  <Button
                    className="bg-red-500 font-medium text-white  "
                    htmlType="submit"
                    // htmlType="submit"
                  >
                    Đăng Nhập
                  </Button>
                  <Button className="bg-[#22A39F] font-medium text-white  ">
                    <NavLink to={"/register"}>Đăng Ký</NavLink>
                  </Button>
                </div>
              </Form.Item>
              <NavLink to={"/"}>
                <button className="text-center font-black bg-[#F3EFE0] ml-28 rounded-3xl px-5 py-2 hover:text-[#22A39F] duration-1000">
                  Về Lại Trang Chủ Nhấn Vào Đây
                </button>
              </NavLink>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
