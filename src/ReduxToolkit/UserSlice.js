import { createSlice } from "@reduxjs/toolkit";
import { userLocalService } from "../Service/LocalService";

const initialState = {
  user: userLocalService.get(),
};

const UserSlice = createSlice({
  name: "UserSlice",

  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const { setUserInfor } = UserSlice.actions;

export default UserSlice.reducer;
