import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import Layout from "./HOC Higher Order Component/Layout/Layout";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:idPhim"
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="*" element={<NotFoundPage></NotFoundPage>} />
         
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
