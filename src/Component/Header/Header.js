import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../../Acset/Logocap.png";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="bg-[#434242] w-screen top-0 shadow-2xl h-[70px] fixed z-10 opacity-90 ">
      <div className="mx-20 px-20 flex justify-between items-center">
        <NavLink to={"/"}>
          <img
            className="w-[100px] hover:opacity-60 duration-1000"
            src={logo}
            alt=""
          />
        </NavLink>
        <UserNav />
      </div>
    </div>
  );
}
