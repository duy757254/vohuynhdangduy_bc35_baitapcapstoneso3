import React from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { userLocalService } from "../../Service/LocalService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.UserSlice.user;
  });
  let handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
  };

  const rendercontent = () => {
    if (user) {
      return (
        <>
          <span className="px-6 py-2 rounded-lg mr-6 bg-[#22A39F] font-medium text-[#434242] hover:text-[#F3EFE0] duration-1000">
            {user?.hoTen}
          </span>
          <button
            onClick={handleLogout}
            className="px-6 py-2 rounded-lg mr-6 bg-[#22A39F] font-medium text-[#434242] hover:text-[#F3EFE0] duration-1000"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="px-6 py-2 rounded-lg mr-6 bg-[#22A39F] font-medium text-[#434242] hover:text-[#F3EFE0] duration-1000">
              Đăng Nhập
            </button>
          </NavLink>
          <NavLink to={"/register"}>
            <button className="px-6 py-2 rounded-lg mr-6 bg-[#22A39F] font-medium text-[#434242] hover:text-[#F3EFE0] duration-1000">
              Đăng Ký
            </button>
          </NavLink>
        </>
      );
    }
  };
  return <div>{rendercontent()}</div>;
}
